import assert from 'assert';
import app from '../../src/app';

describe('\'apartments\' service', () => {
  it('registered the service', () => {
    const service = app.service('apartments');

    assert.ok(service, 'Registered the service');
  });
});
