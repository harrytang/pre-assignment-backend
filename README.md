# pre-assignment

> dwellet pre assignment

## Front-End

- Build with [React](https://reactjs.org/) and [Material UI](https://material-ui.com/).
- Gird listing instead of (rows) table listing.
- Create,update and delete functions
- Notification after create and update actions (success or error)
- Pagination

Live [demo](https://harrytang.github.io/pre-assignment-ui/).

- Why did you choose the actual UI design you implemented?
   
  ```
  I used gird listing which displays all apartment info in one block, allowing users to see all info without the need to
  click on the apartment and go to 'view' page. 
  ```  
## Back-End

- Build with [FeathersJS](https://feathersjs.com/) and [MongoDB (Mongoose)](https://mongoosejs.com/).
- Server site validation
- GitLab CI-CD

Demo [endpoint](https://dwellet-api.harrytang.com/apartments).

- What backend technologies did you use and why? What advantages do your chosen
  technologies have? What possible disadvantages they have?
   
  ```
  I used FeathersJS and Mongoose to build the backend application. This allows me to build the CRUD of a model
  (apartment) in minutes. In addition, FeathersJS and it's ecosystem have almost everthing we might need, such as
  authentication, caching, realtime. Furthermore, It is very flexible, you can even wrap GraphQL to your API. I have not
  found any disadvantages so far.
  ```

- How did you maintain your code quality?
    
  ```
  I have many ways to maintain the code quality, typescript is one of them. Besides, I also use code quality metrics
  tools like codeclimate, greenkeeper. Writing tests also plays an important role in code quality improment.
  ```

## Back-End deployment
The easiest way to deploy is using docker, here an example of docker-compose config:
```yaml
version: '3.7'

services:
  backend:
    image: registry.gitlab.com/harrytang/pre-assignment-backend:latest
    environment:
      - NODE_ENV=production
      - DB_URI=mongodb+srv://mongodb.powerkernel.net/dwellet_db
      - DB_USER=dwellet
      - DB_PASS=VERY_LONG_AND_SECURE_PASS
    ports:
      - target: 3030
        published: 3030
        protocol: tcp
```
For running locally, create an `.env` file with the following content
```
DB_URI=mongodb://localhost/dwellet_db
DB_USER=your_local_user
DB_PASS=your_local_pass

```
then start the application
```
npm run dev
```
## Auto DevOps
This demo is "Auto DevOps", which means there is an auto deployment if there is any new tag pushed to the git repository.

The process is as below:

1. New tag pushed
2. GitLab pipeline triggered.
3. If the pipeline success (after testing passed and docker image built), a webhook send to AWS to perform the deployment.
