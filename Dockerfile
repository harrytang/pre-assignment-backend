FROM node:lts-alpine
# GitLab environment variables
ARG DB_USER
ARG DB_PASS
# app directory
WORKDIR /usr/src/app
# install npm dependencies
COPY ./package*.json ./
RUN npm install
# copy app source
COPY . .
# form test DB user & pass from GitLab environment variables
RUN touch ./.env
RUN echo "DB_URI=${DB_URI}" >> ./.env
RUN echo "DB_USER=${DB_USER}" >> ./.env
RUN echo "DB_PASS=${DB_PASS}" >> ./.env
# test
RUN npm test
RUN rm ./.env
# start
EXPOSE 3030
CMD ["npm", "run", "start"]
