import mongoose from 'mongoose';
import {Application} from './declarations';
import logger from './logger';

export default function (app: Application) {
  mongoose.connect(
    process.env.DB_URI || app.get('mongodb'),
    {
      user: process.env.DB_USER,
      pass:  process.env.DB_PASS,
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  ).catch(err => {
    logger.error(err);
    process.exit(1);
  });

  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
}
