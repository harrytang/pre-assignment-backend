// apartments-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import {Application} from '../declarations';

export default function (app: Application) {
  const mongooseClient = app.get('mongooseClient');
  const {Schema} = mongooseClient;
  const apartments = new Schema({
    address: {type: String, required: true, trim: true},
    postalCode: {type: String, required: true, minlength: 5, maxlength: 6, trim: true},
    postalArea: {type: String, required: true, trim: true},
    rooms: {type: String, required: true, trim: true},
    squareMeters: {type: Number, required: true},
    buildYear: {type: Number, required: true, max: new Date().getFullYear()},
    hasElevator: {type: Boolean, required: false},
    price: {type: Number, required: true, min: 0},
    notes: {type: String, required: false, trim: true},
  }, {
    timestamps: true
  });

  return mongooseClient.model('apartments', apartments);
}
