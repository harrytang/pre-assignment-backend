// Initializes the `apartments` service on path `/apartments`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Apartments } from './apartments.class';
import createModel from '../../models/apartments.model';
import hooks from './apartments.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'apartments': Apartments & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/apartments', new Apartments(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('apartments');

  service.hooks(hooks);
}
